const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    lastName: { type: String },
    phone: { type: Number },
    postalCode: { type: Number },
    street: {type: String },
    poblation: { type: String },
    country: {enum:[ "Spain", "Portugal", "Andorra"],
              required : true },
    photo: {type: String },
    donations: {type: mongoose.Types.ObjectId,
                ref: 'donations' },
    pickUp: {type: mongoose.Types.ObjectId, 
              ref: 'pickUps'}
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model("users", userSchema);

module.exports = User;
